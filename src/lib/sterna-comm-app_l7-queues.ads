--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: Apache-2.0
--  SPDX-FileCopyrightText: Copyright 2023 STERNA MARINE sas (william.franck@sterna.io)
--  SPDX-Creator: William J. Franck (william.franck@sterna.io)
--  --------------------------------------------------------------------------------------
--  Initial creation date : 2023-03-09
--  --------------------------------------------------------------------------------------
--
--  @summary: Messages Queues and Event Sourcing pattern demo
--
--  @description
--
--  --------------------------------------------------------------------------------------

with Sterleardemoqueu_Config;

with System;
with Ada.Containers;
with Ada.Containers.Synchronized_Queue_Interfaces;
with Ada.Containers.Bounded_Synchronized_Queues;

use Ada;

generic
   type Element_Type is private;
   Queue_Size : Containers.Count_Type 
      := Containers.Count_Type (Sterleardemoqueu_Config.Queue_Size);

package Sterna.Comm.App_L7.Queues is
   
   package Message_Queue_Interface is
      new Containers.Synchronized_Queue_Interfaces (Element_Type => Element_Type);

   myDefault_Ceiling : System.Any_Priority := System.Priority'Last;

   package Msg_Queues is new Containers.Bounded_Synchronized_Queues (
         Queue_Interfaces => Message_Queue_Interface,
         Default_Capacity => Queue_Size,
         Default_Ceiling  => myDefault_Ceiling);

end Sterna.Comm.App_L7.Queues;
