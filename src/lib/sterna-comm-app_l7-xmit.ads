pragma Wide_Character_Encoding (UTF8);
--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: Apache-2.0
--  SPDX-FileCopyrightText: Copyright 2023 STERNA MARINE sas (william.franck@sterna.io)
--  SPDX-Creator: William J. Franck (william.franck@sterna.io)
--  --------------------------------------------------------------------------------------
--  Initial creation date : 2023-03-09
--  --------------------------------------------------------------------------------------
--
--  @summary: Messages Queues and Event Sourcing pattern demo
--
--  @description
--
--  --------------------------------------------------------------------------------------

with Sterna.Demo.Domain.Events.Event_001.Queues;
use Sterna.Demo.Domain.Events.Event_001.Queues;

package Sterna.Comm.App_L7.Xmit is

   --  ------
   --  Get --
   --  ------
   function Get_Producer_Queue return access Q_001.Msg_Queues.Queue;

   function Get_Consumer_Queue return access Q_001.Msg_Queues.Queue;

   task Monitor is
      entry Stop;
      entry Wait_for_Stop;
   end Monitor;

   task Network_Pipe is
      entry Start;
   end Network_Pipe;

end Sterna.Comm.App_L7.Xmit;
