--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: Apache-2.0
--  SPDX-FileCopyrightText: Copyright 2023 STERNA MARINE sas (william.franck@sterna.io)
--  SPDX-Creator: William J. Franck (william.franck@sterna.io)
--  --------------------------------------------------------------------------------------
--  Initial creation date : 2023-03-09
--  --------------------------------------------------------------------------------------
--
--  @summary: Messages Queues and Event Sourcing pattern demo
--
--  @description
--
--  --------------------------------------------------------------------------------------

with Sterna.Comm.App_L7.Queues;

with Sterleardemoqueu_Config;

with Ada.Containers;
use Ada.Containers;

package Sterna.Demo.Domain.Events.Event_001.Queues is

   use Ada;

   Queue_Size : constant Containers.Count_Type := Sterleardemoqueu_Config.Queue_Size;

   package Q_001 is
      new Sterna.Comm.App_L7.Queues (
         Element_Type => Message,
         Queue_Size => Queue_Size);

end Sterna.Demo.Domain.Events.Event_001.Queues;
