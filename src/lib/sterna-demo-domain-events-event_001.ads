--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: Apache-2.0
--  SPDX-FileCopyrightText: Copyright 2023 STERNA MARINE sas (william.franck@sterna.io)
--  SPDX-Creator: William J. Franck (william.franck@sterna.io)
--  --------------------------------------------------------------------------------------
--  Initial creation date : 2023-03-09
--  --------------------------------------------------------------------------------------
--
--  @summary: Messages Queues and Event Sourcing pattern demo
--
--  @description
--
--  --------------------------------------------------------------------------------------


package Sterna.Demo.Domain.Events.Event_001 is

   type Money is delta 0.01 digits 12;
   type Currency_Kind is (EUR, USD, CHF, UKP);
   subtype Text_64 is Wide_Wide_String (1 ..64);


   type Message is new Root_Message with record
      B : Boolean := False;
      I : Integer := 0;
      F : Float := 0.0;
      D : Money := 0.0;
      Currency : Currency_Kind;
      Description : Text_64;
   end record;

end Sterna.Demo.Domain.Events.Event_001;
