--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: Apache-2.0
--  SPDX-FileCopyrightText: Copyright 2023 STERNA MARINE sas (william.franck@sterna.io)
--  SPDX-Creator: William J. Franck (william.franck@sterna.io)
--  --------------------------------------------------------------------------------------
--  Initial creation date : 2023-03-09
--  --------------------------------------------------------------------------------------
--
--  @summary: Messages Queues and Event Sourcing pattern demo
--
--  @description
--
--  --------------------------------------------------------------------------------------

-- with Ada.Calendar;

package Sterna.Demo.Domain.Events is

   subtype Agent_ID is Positive;
   subtype API_version is Natural;
   subtype Msg_ID is Natural;

   type Root_Message is tagged record
      Version   : API_version := 0;
      ID        : Msg_ID := 0;
      Sender_ID : Agent_ID;
--    Created_on : Calendar.Time;
   end record;

end Sterna.Demo.Domain.Events;
