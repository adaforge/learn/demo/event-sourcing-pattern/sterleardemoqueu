package Sterna.Services.Transversal.Synchronized_Trace is

   protected Trace_IO is
      procedure Put (item : Wide_Wide_String);
      procedure Put_Line (item : Wide_Wide_String);
   end Trace_IO;

end Sterna.Services.Transversal.Synchronized_Trace;