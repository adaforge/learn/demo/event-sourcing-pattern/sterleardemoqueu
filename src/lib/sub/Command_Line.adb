--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: Apache-2.0
--  SPDX-FileCopyrightText: Copyright 2020 STERNA MARINE s.a.s. (william.franck@sterna.io)
--  SPDX-Creator: William J. FRANCK (william.franck@sterna.io)
--  --------------------------------------------------------------------------------------
--  Initial creation date : 2020-01-10
--  --------------------------------------------------------------------------------------

with Ada.Command_Line;

with Ada.Characters.Latin_1;
-- with Ada.Characters.Handling;

with Ada.Text_IO;

use Ada;
use Ada.Characters;

package body Command_Line is

   --  --------
   --  Get_Args
   --  --------
   procedure Get_Args (Args : in out Program_args) is

      package ACL renames Ada.Command_Line;

   begin
      if ACL.Argument_Count = 1 then
         case ACL.Argument (1)(2) is
            when 'h' =>
               raise BAD_ARGUMENTS;

            when 't' =>
               Args.Trace := True;

            when 'T' =>
               Args.Timings := True;
            when others =>
               raise BAD_ARGUMENTS;
         end case;
      end if;

   exception
      when BAD_ARGUMENTS =>
         Text_IO.New_Line (Text_IO.Standard_Error);
         Text_IO.Put_Line
           (Text_IO.Standard_Error,
            "Usage : " & ACL.Command_Name & " [ -t | -T ] file_name ");
         Text_IO.Put_Line (Text_IO.Standard_Error, "Options:");
         Text_IO.Put_Line
           (Text_IO.Standard_Error, Latin_1.HT & "-t --trace   : Trace_IO of intermediate results");
         Text_IO.Put_Line
           (Text_IO.Standard_Error, Latin_1.HT & "-T --timings : Trace_IO of intermediate results");
         Text_IO.New_Line (Text_IO.Standard_Error);
         ACL.Set_Exit_Status (ACL.Failure);
         raise;
   end Get_Args;

end Command_Line;
