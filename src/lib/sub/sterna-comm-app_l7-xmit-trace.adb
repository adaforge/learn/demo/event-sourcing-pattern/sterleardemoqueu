pragma Wide_Character_Encoding (UTF8);
--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: Apache-2.0
--  SPDX-FileCopyrightText: Copyright 2023 STERNA MARINE sas (william.franck@sterna.io)
--  SPDX-Creator: William J. Franck (william.franck@sterna.io)
--  --------------------------------------------------------------------------------------
--  Initial creation date : 2023-03-09
--  --------------------------------------------------------------------------------------
--
--  @summary: Messages Queues and Event Sourcing pattern demo
--
--  @description
--
--  --------------------------------------------------------------------------------------

with Sterna.Services.Transversal.Synchronized_Trace;
with Command_Line;
with Sterleardemoqueu_Config;
with Ada.Strings.Wide_Wide_Fixed;

separate (Sterna.Comm.App_L7.Xmit)
package body Trace is

   use Sterna.Services.Transversal.Synchronized_Trace;
   use Sterleardemoqueu_Config;
   use Ada.Strings.Wide_Wide_Fixed;

   procedure Advice (CrossPoint : JoinPoint) is
      Indent : constant Wide_Wide_String := 9 * ' ';
   begin
      if Command_Line.Run_Args.Trace then -- Trace
         case CrossPoint is

            when Open_the_Network =>
               Trace_IO.Put_Line (Indent & "Open the Network pipe");

            when Message_on_the_Network =>
               Msg_Count := @ + 1;
               if Sterleardemoqueu_Config.Trace_Level in (Debug) then
                  Trace_IO.Put_Line (Indent & CrossPoint'Wide_Wide_Image
                     & " Message_001 "
                     & " #" & Msg_Count'Wide_Wide_Image);

               --  Trace_IO.Put_Line (Indent & "myMessage_001 = ("
               --  & myMessage_001.B'Wide_Wide_Image & ","
               --  & myMessage_001.I'Wide_Wide_Image & ","
               --  & myMessage_001.F'Wide_Wide_Image & ","
               --  & myMessage_001.D'Wide_Wide_Image & ", "
               --  & Currency_Kind'Wide_Wide_Image (myMessage_001.Currency) & ", "
               --  & myMessage_001.Description
               --  & ")");
               end if;

            when Stop_Accepted =>
               Trace_IO.Put_Line (Indent & "Network Stop accepted");

            when Close_the_Network =>
               Trace_IO.Put_Line (Indent & "Close the Network pipe");

         end case;
      end if;
   end Advice;
end Trace;
