pragma Wide_Character_Encoding (UTF8);
--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: Apache-2.0
--  SPDX-FileCopyrightText: Copyright 2023 STERNA MARINE sas (william.franck@sterna.io)
--  SPDX-Creator: William J. Franck (william.franck@sterna.io)
--  --------------------------------------------------------------------------------------
--  Initial creation date : 2023-03-09
--  --------------------------------------------------------------------------------------
--
--  @summary: Messages Queues and Event Sourcing pattern demo
--
--  @description
--
--  --------------------------------------------------------------------------------------

with Sterna.Demo.Domain.Events.Event_001;
use Sterna.Demo.Domain.Events;

package body Sterna.Comm.App_L7.Xmit is

      Producer_Queue : aliased Q_001.Msg_Queues.Queue;
      Consumer_Queue : aliased Q_001.Msg_Queues.Queue;

      Msg_Count : Natural := 0;

   --  ---------------------------  --
   --  Get Producer/Consumer Queue  --
   --  ---------------------------  --
   function Get_Producer_Queue
      return access Q_001.Msg_Queues.Queue
         is (Producer_Queue'Access);

   function Get_Consumer_Queue
      return access Q_001.Msg_Queues.Queue
         is (Consumer_Queue'Access);

   --  --------------  --
   --  Trace «Aspect»  --
   --  --------------  --
   package Trace is
      type JoinPoint is (Open_the_Network, Message_on_the_Network, Stop_Accepted, Close_the_Network);
      procedure Advice (CrossPoint : JoinPoint);
   end Trace;
   package body Trace is separate;
   use Trace;

   --  -------  --
   --  Monitor  --
   --  -------  --
   task body Monitor is
   begin
      accept Stop do
         null;
      end Stop;
      accept Wait_for_Stop do
         null;
      end Wait_for_Stop;
   end Monitor;


   --  ------------  --
   --  Network_Pipe  --
   --  ------------  --
   task body Network_Pipe is

      Some_Message_001 : Event_001.Message;

   begin
      accept Start do
         Trace.Advice (Open_the_Network); -- Trace
      end Start;

      loop
         select
            Monitor.Wait_for_Stop;
            Trace.Advice (Stop_Accepted); -- Trace
            exit;
         then abort
            Producer_Queue.Dequeue (Some_Message_001);
            Trace.Advice (Message_on_the_Network); -- Trace
            Consumer_Queue.Enqueue (Some_Message_001);
         end select;
      end loop;

   end Network_Pipe;

end Sterna.Comm.App_L7.Xmit;
