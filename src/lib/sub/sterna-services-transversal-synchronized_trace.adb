with Ada.Wide_Wide_Text_IO;
use Ada;

package body Sterna.Services.Transversal.Synchronized_Trace is

   protected body Trace_IO is

      procedure Put (Item : Wide_Wide_String) is
      begin
         Wide_Wide_Text_IO.Put (Item);
      end Put;

      procedure Put_Line (item : Wide_Wide_String) is
      begin
         Wide_Wide_Text_IO.Put_Line (Item);
      end Put_Line;

   end Trace_IO;
end Sterna.Services.Transversal.Synchronized_Trace;