--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: Apache-2.0
--  SPDX-FileCopyrightText: Copyright 2023 STERNA MARINE sas (william.franck@sterna.io)
--  SPDX-Creator: William J. Franck (william.franck@sterna.io)
--  --------------------------------------------------------------------------------------
--  Initial creation date : 2023-03-09
--  --------------------------------------------------------------------------------------
--
--  @summary: Messages Queues and Event Sourcing pattern demo
--
--  @description
--
--  --------------------------------------------------------------------------------------

with Sterna.Demo.Domain.Events;
use Sterna.Demo.Domain;

with Sterna.Comm.App_L7.Xmit;
use Sterna.Comm.App_L7;

with Sterna_Learn_Demo_EventSourcing.Producers;
with Sterna_Learn_Demo_EventSourcing.Consumers;
use Sterna_Learn_Demo_EventSourcing;

with Sterna.Services.Transversal.Synchronized_Trace;
with Command_Line;

with Sterleardemoqueu_Config;

--  with Ada.Wide_Wide_Text_IO;
--  use Ada.Wide_Wide_Text_IO;
--  use Ada;

procedure Sterna_Learn_Demo_EventSourcing_Main
is

   Nb_Producers : constant Events.Agent_ID
         := Events.Agent_ID (Sterleardemoqueu_Config.Default_Nb_Producers);

   myProducers : array (Sterna.Demo.Domain.Events.Agent_ID'First .. Nb_Producers) of access Producers.Agent;

   myConsumer : Consumers.Agent;

   --  --------------
   --  Trace_IO «Aspect»
   --  --------------
   package Trace is
      type JoinPoint is (Before_Launching_Tasks, All_Tasks_Ended);
      procedure Advice (CrossPoint : JoinPoint);
   end Trace;
   package body Trace is separate;
   use Trace;

begin
   --  --------------
   --      MAIN
   --  --------------
   --  get the command line arguments
   Command_Line.Get_Args (Args => Command_Line.Run_Args);

--   Wide_Wide_Text_IO.Put_Line (Standard_Error, "Starting main ...");
   for i in myProducers'Range loop
      myProducers (i) := new Producers.Agent (i);
   end loop;

      -- START tasks
   Trace.Advice (Before_Launching_Tasks); -- Trace
   Xmit.Network_Pipe.Start;
   myConsumer.Start;
   for i in myProducers'Range loop
      myProducers (i).Start;
   end loop;

   -- Wait for all producers to stop;
   for i in myProducers'Range loop
      myProducers (i).Stopped;
   end loop;

   -- Wait for Consumers to stop;
   Consumers.Monitor.Stop;

   -- STOP other tasks
   Xmit.Monitor.Stop;
   Trace.Advice (All_Tasks_Ended); -- Trace

--   Wide_Wide_Text_IO.Put_Line (Standard_Error, "Stopping Main ...");

end Sterna_Learn_Demo_EventSourcing_Main;
