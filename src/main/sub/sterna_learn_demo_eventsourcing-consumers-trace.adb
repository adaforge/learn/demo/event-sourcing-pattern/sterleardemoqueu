pragma Wide_Character_Encoding (UTF8);

with Sterna.Services.Transversal.Synchronized_Trace;

with Command_Line;
with Sterleardemoqueu_Config;

with Ada.Strings.Wide_Wide_Fixed;
use Ada.Strings;

separate (Sterna_Learn_Demo_EventSourcing.Consumers)
package body Trace is

   use Sterna.Services.Transversal.Synchronized_Trace;
   use Sterleardemoqueu_Config;
   use Ada.Strings.Wide_Wide_Fixed;

      Msgs_Count : Natural := 0;

   procedure Advice (CrossPoint : JoinPoint) is

      Indent_1 : constant Wide_Wide_String := 6 * ' ';
      Indent_2 : constant Wide_Wide_String := Indent_1 & 3 * ' ';
      Indent_3 : constant Wide_Wide_String := Indent_2 & 3 * ' ';
   begin
      if Command_Line.Run_Args.Trace then -- Trace
         case CrossPoint is

            when After_Task_Launch =>
               Trace_IO.Put_Line (Indent_1 & "Task Consumer launched …");

            when Start_Accepted =>
               Trace_IO.Put_Line (Indent_2 & "Consumers.Start entry Accepted …");

            when After_Start =>
               if Sterleardemoqueu_Config.Trace_Level in (Debug) then
                  Trace_IO.Put_Line (Indent_3 & "End of RdV with Start …");
               end if;

            when Got_Message =>
               Msgs_Count := @ + 1;
               if Sterleardemoqueu_Config.Trace_Level in (Debug) then
                  Trace_IO.Put_Line (Indent_3 & JoinPoint'Wide_Wide_Image(CrossPoint) & " myMessage_001 "
                     & "v" & Wide_Wide_Fixed.Trim (myMessage_001.Version'Wide_Wide_Image, Left)
                     & "["& Wide_Wide_Fixed.Trim (myMessage_001.Sender_Id'Wide_Wide_Image, Left)  & ":"
                     &  Wide_Wide_Fixed.Trim (myMessage_001.ID'Wide_Wide_Image, Left)  & "] = "
                     & " (" & myMessage_001.B'Wide_Wide_Image & ","
                     & myMessage_001.I'Wide_Wide_Image & ","
                     & myMessage_001.F'Wide_Wide_Image & ","
                     & myMessage_001.D'Wide_Wide_Image & ", "
                     & Event_001.Currency_Kind'Wide_Wide_Image (myMessage_001.Currency) & ", "
                     & myMessage_001.Description
                     & ")");
               end if;

            when Stop_Accepted =>
               if Sterleardemoqueu_Config.Trace_Level in (Debug) then
                  Trace_IO.Put_Line (Indent_2 & "Consumers.Stop entry Accepted …");
               end if;

            when After_Stop =>
               if Sterleardemoqueu_Config.Trace_Level in Debug | Info then
                  Trace_IO.Put_Line (Indent_2 & "Total Nb of messages received = "
                     & Msgs_Count'Wide_Wide_Image);
                  Trace_IO.Put_Line (Indent_2
                     & "Peak  Nb of messages in Consumer Queue = " & Natural (Peak_Queue_Use)'Wide_Wide_Image);
               end if;

         end case;
      end if;
   end Advice;
end Trace;