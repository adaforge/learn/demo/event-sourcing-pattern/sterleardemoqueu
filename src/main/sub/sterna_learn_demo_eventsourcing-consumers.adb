--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: Apache-2.0
--  SPDX-FileCopyrightText: Copyright 2023 STERNA MARINE sas (william.franck@sterna.io)
--  SPDX-Creator: William J. Franck (william.franck@sterna.io)
--  --------------------------------------------------------------------------------------
--  Initial creation date : 2023-03-09
--  --------------------------------------------------------------------------------------
--
--  @summary: Messages Queues and Event Sourcing pattern demo
--
--  @description
--
--  --------------------------------------------------------------------------------------

with Sterna.Demo.Domain.Events.Event_001;
with Sterna.Demo.Domain.Events.Event_001.Queues;
use Sterna.Demo.Domain.Events;

with Sterna.Comm.App_L7.Xmit;

with Ada.Containers;
use Ada.Containers;
use Ada;

--  with Ada.Wide_Wide_Text_IO;
--  use Ada.Wide_Wide_Text_IO;

package body Sterna_Learn_Demo_EventSourcing.Consumers is

   myMessage_001  : Event_001.Message;
   Peak_Queue_Use : Containers.Count_Type;

   --  --------------
   --  Trace «Aspect»
   --  --------------
   package Trace is
      type JoinPoint is (After_Task_Launch, Start_Accepted, Got_Message, After_Start, Stop_Accepted, After_Stop);
      procedure Advice (CrossPoint : JoinPoint);
   end Trace;
   package body Trace is separate;
   use Trace;

   --  ------   --
   --  Monitor  --
   --  -------  --
   task body Monitor is
   begin
      accept Stop do
         null;
      end Stop;
      accept Wait_for_Stop do
         null;
      end Wait_for_Stop;
   end Monitor;

   --  -----  --
   --  Agent  --
   --  -----  --
   task body Agent is

      Consumer_Queue : access Event_001.Queues.Q_001.Msg_Queues.Queue;
      Msgs_in_Queue : Containers.Count_Type;

   begin
      Trace.Advice (After_Task_Launch); -- Trace
      accept Start do
         null;
         Trace.Advice (Start_Accepted); -- Trace
      end Start;

      Consumer_Queue := Sterna.Comm.App_L7.Xmit.Get_Consumer_Queue;

      loop
         while Consumer_Queue.Current_Use > 0 loop
               Consumer_Queue.Dequeue (myMessage_001);
               Trace.Advice (Got_Message); -- Trace
               -- Trace_IO.Put (Standard_Error, "!");
         end loop;

         if Consumer_Queue.Current_Use = 0 then
            select
               Monitor.Wait_for_Stop;
               Peak_Queue_Use := Consumer_Queue.Peak_Use;
               Trace.Advice (Stop_Accepted); -- Trace
               exit;
            then abort
               null;
            end select;
         end if;

      end loop;

      Msgs_in_Queue  := Consumer_Queue.Current_Use;
      if Msgs_in_Queue > 0 then
         raise CONSUMER_QUEUE_NOT_EMPTY with "Consumer has still" & Msgs_in_Queue'Image & "messages in queue !";
      end if;

      Trace.Advice (After_Stop); -- Trace
   end Agent;

end Sterna_Learn_Demo_EventSourcing.Consumers;
