pragma Wide_Character_Encoding (UTF8);

with Command_Line;
with Sterleardemoqueu_Config;

separate (Sterna_Learn_Demo_EventSourcing.Producers)
package body Trace is

   use Sterna.Services.Transversal.Synchronized_Trace;
   use Sterleardemoqueu_Config;
   use Ada.Strings.Wide_Wide_Fixed;

   procedure Advice (ID : Sterna.Demo.Domain.Events.Agent_ID; CrossPoint : JoinPoint) is
      Indent_1 : constant Wide_Wide_String := 6 * ' ';
      Indent_2 : constant Wide_Wide_String := Indent_1 & 3 * ' ';
      Indent_3 : constant Wide_Wide_String := Indent_2 & 3 * ' ';
   begin
      if Command_Line.Run_Args.Trace then -- Trace
         case CrossPoint is

            when After_Task_Launch =>
               Trace_IO.Put_Line (Indent_1 & "Task Producer [" & ID'Wide_Wide_Image & "] launched ...");

            when Start_Accepted =>
               Trace_IO.Put_Line (Indent_2 & "[" & ID'Wide_Wide_Image & "] Producers.Start entry Accepted ...");
               Trace_IO.Put_Line (Indent_3 & "[" & ID'Wide_Wide_Image & "] myMessage_001 = ("
               & myMessage_001.B'Wide_Wide_Image & ","
               & myMessage_001.I'Wide_Wide_Image & ","
               & myMessage_001.F'Wide_Wide_Image & ","
               & myMessage_001.D'Wide_Wide_Image & ", "
               & Event_001.Currency_Kind'Wide_Wide_Image (myMessage_001.Currency) & ", "
               & myMessage_001.Description
               & ")");

            when After_Start =>
               Trace_IO.Put_Line (Indent_3 & "[" & ID'Wide_Wide_Image & "] Start producing ...");

            when Msg_on_Queue =>
               if Sterleardemoqueu_Config.Trace_Level in (Debug) then
                  Trace_IO.Put_Line (Indent_3 & "[" & ID'Wide_Wide_Image  & "] "
                     & JoinPoint'Wide_Wide_Image (CrossPoint));
               end if;

            when Stop_Accepted =>
               Trace_IO.Put_Line (Indent_2 & "[" & ID'Wide_Wide_Image & "] Producers.Stop entry Accepted ...");

            when After_Stop =>
               Trace_IO.Put_Line (Indent_3 & "[" & ID'Wide_Wide_Image & "] End of RdV with Stop ...");

            when Stopped =>
                  Trace_IO.Put_Line (Indent_2 & "[" & ID'Wide_Wide_Image & "] Stopped");

         end case;
      end if;
   end Advice;
end Trace;