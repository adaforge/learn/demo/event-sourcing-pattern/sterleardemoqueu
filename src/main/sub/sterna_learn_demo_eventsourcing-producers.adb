--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: Apache-2.0
--  SPDX-FileCopyrightText: Copyright 2023 STERNA MARINE sas (william.franck@sterna.io)
--  SPDX-Creator: William J. Franck (william.franck@sterna.io)
--  --------------------------------------------------------------------------------------
--  Initial creation date : 2023-03-09
--  --------------------------------------------------------------------------------------
--
--  @summary: Messages Queues and Event Sourcing pattern demo
--
--  @description
--
--  --------------------------------------------------------------------------------------

with Sterna.Demo.Domain.Events.Event_001;
with Sterna.Demo.Domain.Events.Event_001.Queues;
use Sterna.Demo.Domain.Events;

with Sterna.Comm.App_L7.Xmit;
use Sterna.Comm.App_L7;

with Sterna.Services.Transversal.Synchronized_Trace;
with Sterleardemoqueu_Config;

with Ada.Containers;
use Ada.Containers;
with Ada.Strings.Wide_Wide_Fixed;
use Ada;

package body Sterna_Learn_Demo_EventSourcing.Producers is

   use Event_001;

   myMessage_001 : Event_001.Message := (
      Version => 1,
      Sender_ID => <>,
      ID => <>,
--    Created_on : Calendar.Time;
      B => True,
      I => 1_000,
      F => 99.9,
      D => 1_999.99,
      Currency => EUR,
      Description => (others => 'X'));

   --  ------   --
   --  Monitor  --
   --  -------  --
   --  task body Monitor is
   --  begin
      --  accept Stop do
      --     null;
      --  end Stop;
      --  accept Wait_for_Stop do
      --     null;
      --  end Wait_for_Stop;
   --  end Monitor;


   --  --------------
   --  Trace «Aspect»
   --  --------------
   package Trace is
      type JoinPoint is (
            After_Task_Launch,
            Start_Accepted,
            After_Start,
            Msg_on_Queue,
            Stop_Accepted,
            After_Stop,
            Stopped);

      procedure Advice (ID : Sterna.Demo.Domain.Events.Agent_ID; CrossPoint : JoinPoint);
   end Trace;
   package body Trace is separate;
   use Trace;

   --  --------
   --  Agent --
   --  --------
   task body Agent is

      Producer_Queue : access Event_001.Queues.Q_001.Msg_Queues.Queue;
      -- Msgs_in_Queue : Containers.Count_Type;

      Peak_Queue_Use : Containers.Count_Type;

      use Sterna.Services.Transversal.Synchronized_Trace;
      use Sterleardemoqueu_Config;
      use Ada.Strings.Wide_Wide_Fixed;

   begin
      Trace.Advice (ID, After_Task_Launch); -- Trace
      accept Start do
         null;
      end Start;
      Trace.Advice (ID, After_Start); -- Trace

      Producer_Queue := Xmit.Get_Producer_Queue;
      for i in 1 .. Sterleardemoqueu_Config.Default_Nb_Loops loop

         myMessage_001.Sender_ID := ID;
         myMessage_001.ID := i;

         Producer_Queue.Enqueue (myMessage_001);
         Trace.Advice (ID, Msg_on_Queue); -- Trace
      end loop;

      accept Stopped do
         Trace.Advice (ID, Stopped); -- Trace

         Peak_Queue_Use := Producer_Queue.Peak_Use;
         if Sterleardemoqueu_Config.Trace_Level in Debug | Info then
            Trace_IO.Put_Line ((9 * ' ') & "Peak  Nb of messages in Producer Queue ["
               & ID'Wide_Wide_Image & "] = "
               & Natural (Peak_Queue_Use)'Wide_Wide_Image);
         end if;

         --  Msgs_in_Queue := Producer_Queue.Current_Use;
         --  if Msgs_in_Queue > 0 then
         --     raise PRODUCER_QUEUE_NOT_EMPTY
         --        with "Producer [" & ID'Image & "] has still" & Msgs_in_Queue'Image & "messages in queue !";
         --  end if;
      end Stopped;

   end Agent;

end Sterna_Learn_Demo_EventSourcing.Producers;
