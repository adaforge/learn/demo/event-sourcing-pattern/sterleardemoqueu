--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: Apache-2.0
--  SPDX-FileCopyrightText: Copyright 2023 STERNA MARINE sas (william.franck@sterna.io)
--  SPDX-Creator: William J. Franck (william.franck@sterna.io)
--  --------------------------------------------------------------------------------------
--  Initial creation date : 2023-03-09
--  --------------------------------------------------------------------------------------
--
--  @summary: Messages Queues and Event Sourcing pattern demo
--
--  @description
--
--  --------------------------------------------------------------------------------------

with Sterna.Demo.Domain.Events;

package Sterna_Learn_Demo_EventSourcing.Producers is

   PRODUCER_QUEUE_NOT_EMPTY : exception;

   --  task Monitor is
   --     entry Stop;
   --     entry Wait_for_Stop;
   --  end Monitor;

   task type Agent (ID : Sterna.Demo.Domain.Events.Agent_ID) is
      entry Start;
      entry Stopped;
   end Agent;

end Sterna_Learn_Demo_EventSourcing.Producers;
