with Sterleardemoqueu_Config;
with Ada.Strings.Wide_Wide_Fixed;

separate (Sterna_Learn_Demo_EventSourcing_Main)
package body Trace is

   use Sterna.Services.Transversal.Synchronized_Trace;
   use Sterleardemoqueu_Config;
   use Ada.Strings.Wide_Wide_Fixed;

   procedure Advice (CrossPoint : JoinPoint) is
         Indent : constant Wide_Wide_String := 3 * ' ';
   begin
      if Command_Line.Run_Args.Trace then -- Trace
         case CrossPoint is

            when Before_Launching_Tasks =>
               Trace_IO.Put_Line (Indent & Before_Launching_Tasks'Wide_Wide_Image);

            when All_Tasks_Ended =>
               Trace_IO.Put_Line (Indent & All_Tasks_Ended'Wide_Wide_Image);

         end case;
      end if;
   end Advice;
end Trace;